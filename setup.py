from setuptools import setup
from os import path

# Get the long description from the README file
with open(path.join(path.dirname(__file__), 'Readme.rst'), 'r') as f:
    long_description = f.read()

setup(
    name='pyfeed2imap',
    use_scm_version=True,  # This will generate the version number from git tags

    description='Monitor list of RSS feeds and send updates to email',
    long_description=long_description,
    url='https://gitlab.com/alelec/pyfeed2imap',
    author='Andrew Leech',
    author_email='andrew@alelec.net',
    license='MIT',
    py_modules=['pyfeed2imap'],
    setup_requires=['setuptools_scm'],
    install_requires=['requests', 'feedparser', 'setuptools_scm', 'click', 'structured_config', 'Cython', 'unqlite'],
    entry_points={
        'console_scripts': [
            'pyfeed2imap=pyfeed2imap:main',
        ],
    },
)
