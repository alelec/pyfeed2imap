#!/usr/bin/env python3
import getpass
import socket
import time
import json
import html
import click
import logging
import feedparser
from unqlite import UnQLite
from os.path import expandvars

import sys
from structured_config import Structure, List, TypedField

import imaplib
import email.message


class RssFeed(Structure):
    name = "Lifehacker"
    url = "http://feeds.lifehacker.com.au/lifehackeraustralia"

    host = "imap.zoho.com"
    user = "user"
    password = "password"
    folder = "INBOX"


class Config(Structure):
    check_interval = 0  # If set, will run as service checking every x seconds
    database = TypedField('$HOME/.pyfeed2imap.db', expandvars)

    feeds = List([
        RssFeed()
    ])


@click.command()
@click.option('-c', '--config', default='$HOME/.pyfeed2imap.yaml', help='Path to config file')
def main(config):
    log = logging.getLogger('pyfeed2imap')
    out_hdlr = logging.StreamHandler(sys.stdout)
    out_hdlr.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
    out_hdlr.setLevel(logging.INFO)
    log.addHandler(out_hdlr)
    log.setLevel(logging.INFO)

    log.critical("Startup")

    config_path = expandvars(config)
    config = Config(config_path)
    db = UnQLite(config.database)

    start = time.time()
    while True:
        for feed in config.feeds:
            try:
                assert isinstance(feed, RssFeed)

                rss = feedparser.parse(feed.url)
                feed_name = rss['feed']['title']
                feed_url = rss['feed']['link']

                for entry in rss.entries:
                    try:
                        content = ''.join([h['value'] for h in entry['content']])
                    except KeyError:
                        content = entry['summary']
                    url = entry['link']
                    title = entry['title']
                    author = ": %s" % entry['author'] if 'author' in entry else ""
                    date = entry['published']
                    entry_id = ":".join((feed_name, entry['id']))

                    if entry_id not in db:

                        user = getpass.getuser()
                        hostname = socket.getfqdn()
                        email_addr = '"{feed_name}" <{user}@{hostname}>'.format(**locals())

                        new_message = email.message.EmailMessage()
                        new_message['Subject'] = html.unescape(title)
                        new_message['From'] = email_addr
                        new_message['To'] = email_addr

                        message_content = '<div style="background-color: lightgray; border: 1px solid black; padding: 6px">' \
                                          '<a style="display: block;color:#000099;font-weight: bold;margin-bottom:6px;" href="{url}">{title}</a>' \
                                          '<a style="display: block;color:#000099;font-weight: semibold;" href="{feed_url}">{feed_name}{author}</a>' \
                                          '</div><p>{content}</p>' \
                                          '<div style="background-color: lightgray;color:#484848; border: 1px solid black; padding: 3px; padding-left: 6px;">' \
                                          'Date: {date}' \
                                          '</div>'.format(**locals())

                        new_message.add_alternative(message_content, subtype='html')

                        connection = imaplib.IMAP4_SSL(feed.host)
                        connection.login(feed.user, feed.password)

                        try:
                            connection.append(feed.folder, '',
                                              imaplib.Time2Internaldate(time.time()),
                                              bytes(new_message))

                            with db.transaction():
                                db[entry_id] = json.dumps(entry)
                        finally:
                            try:
                                connection.close()
                            except:
                                pass
                            connection.logout()
            except:
                log.exception("Error getting/parsing feed: %s" % feed.name)

        if not config.check_interval:
            break

        # Wait for rest of interval
        time.sleep(max(1, (start + config.check_interval) - time.time()))


if __name__ == '__main__':
    main()
