pyfeed2imap
===========

Monitor list of RSS feeds and send updates to email.

Inspired by the venerable feed2imap which was written in ruby, I whipped this up to add filtering to prevent duplicates
and avoid the semi-regular cache wipes on crash that I get with feed2imap.